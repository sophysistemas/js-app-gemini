import { Router, Request, Response } from "express";
import { getGemini } from "../services/index";

const routes = Router();

routes.get('/', (request: Request, response: Response) => {
  return response.status(200).json({ res: "OK" });
});

routes.get('/gemini', async(request: Request, response: Response) => {
  const data = await getGemini();
  return response.status(200).json(data);
});


export default routes;