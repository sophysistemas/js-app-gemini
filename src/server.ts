import Express from "express";
import routes from './routes';

require('dotenv').config();

const app = Express();
app.use(routes);

const port = process.env.PORT;

app.listen(port, () => console.log(`🚀 Server running on port ${port}`));
