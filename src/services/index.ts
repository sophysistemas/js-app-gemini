import { GoogleGenerativeAI } from "@google/generative-ai";
require('dotenv').config();


export const getGemini = async () => {
  const api_key = process.env.GOOGLE_API_KEY as string;

  const genAI = new GoogleGenerativeAI(api_key);
  const model = genAI.getGenerativeModel({ model: "gemini-1.5-flash"});

  const prompt = "Escreva um breve resumo sobre Alan Turing";

  const result = await model.generateContent(prompt);
  const response = await result.response;
  const text = response.text();

  return { response: text };
}